const { NlpManager } = require("node-nlp");

const express = require('express');
const bodyParser = require("body-parser");
const cors = require('cors');

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use('/static', express.static(__dirname + '/public'));

const manager = new NlpManager({ languages: ["fr"] });

manager.load();

const misunderstood = [
    "Oops, Je ne comprends pas ce que vous dites..., pouvez-vous vérifier l'orthographe ?",
    "Aïe, ça je ne connais pas !",
    "Je n'ai pas bien saisi x..x, pouvez vous pouvez-vous vérifier l'orthographe ?"
]

app.post('/askbot', async (req, res) => {

    const response = await manager.process("fr", req.body.question);
    if(response.answer)
        res.json(response.answer);
    else
        res.json({
            "sender": "Bot",
            "message": misunderstood.at(Math.floor(Math.random() * misunderstood.length)),
            "data": null,
            "dateType": null
        })

});


app.listen(process.env.port || 3000);