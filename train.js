const { NlpManager } = require("node-nlp");

const manager = new NlpManager({ languages: ["fr"] });

const fs = require("fs");

const files = fs.readdirSync("./intents");

for (const file of files) {
    let data = fs.readFileSync(`./intents/${file}`);
    data = JSON.parse(data);
    const intent = file.replace(".json", "");
    for (const keyword of data.keywords) {
        manager.addDocument("fr", keyword, intent);
    }
    for (const answer of data.answers) {
        manager.addAnswer("fr", intent, answer);
    }
}

async function train_save(){
    await manager.train();
    manager.save();
}

train_save();